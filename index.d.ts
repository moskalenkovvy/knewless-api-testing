export {};
declare global {
    namespace NodeJS {
        interface Global {
            appConfig: {
                envName: string;
                baseUrl: string;
                swagger: string;
                users: object;
            };
        }
    }
}