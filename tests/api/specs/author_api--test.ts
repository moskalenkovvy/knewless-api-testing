import { expect } from "chai";
import { ArticlesController } from "../lib/controllers/articles.controller";
import { AuthorsController } from "../lib/controllers/authors.controller";
import { UserController } from "../lib/controllers/user.controller";
const user = new UserController();
const author = new AuthorsController();
const articles = new ArticlesController();
const schemas = require('./data/shemas_student.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Author controller", () => {
    let token: string;
    let author_data;
    let authorId: string;
    let id: string;
    let authorFirstName: string;
    let authorLastName: string;
    let articleId: string;

    it(`logIn`, async () => {
        let response = await user.logIn({ email: 'valmos.steam@gmail.com', password: 'qwerty123' });
 
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);


        token = response.body["accessToken"];
        //console.log(token)
    });

    it(`getUser`, async () => {
        let response = await user.getUser(token);
        
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_user);

    });

    it(`getAuthor`, async () => {
        let response = await author.getAuthor(token);

        //console.log(response.body);
            
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_author);

        author_data = response.body;
        console.log(author_data["id"]);
    });

    it(`postAuthor`, async () => {
        author_data["job"] = "adfsgmf,lef";
        let response = await author.postAuthor(token, author_data);

        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);

    });

    it(`getAuthorById`, async () => {
        let response = await author.getAuthorById(token, author_data["id"]);
 
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);

    });

    it(`postArticle`, async () => {
        let article_data = {
            "authorId": author_data["id"],
            "authorName": `${author_data["firstName"]} ${author_data["lastName"]}`,
            //"id": author_data["userId"],
            "image": "",
            "name": "Tiiiiitllldddddeewee",
            "text": "tewxxxqrwxxtttftxtxttedattxtdsfgrttcxvghttdtdx sthjwdtcst txdttvsd tstd dtscgsca"
        }
        //console.log(response.body);
        let response = await articles.postArticle(token, article_data);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);

    });

    it(`getMyArticles`, async () => {
        let response = await articles.getMyArticles(token);
 
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_articleAuth);

        articleId = response.body[0]["id"]
    });

    it(`getArticleById`, async () => {
        let response = await articles.getArticleById(token, articleId);
 
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_article);

    });

    it(`postArticleComment`, async () => {
        let articleComm_data = {
            "articleId": articleId,
            "id": id,
            "text": "strsadqfqfwqqing"
          };
        let response = await articles.postArticleComment(token, articleComm_data);
 
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);

    });

    it(`getCommentsOfArticle`, async () => {
        let response = await articles.getArticleComment(token, articleId);
 
        //console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_articleComments);

    });
    
});
