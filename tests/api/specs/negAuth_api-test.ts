import { checkResponseTime, 
    checkStatusCode, 
    checkResponseBodyStatus,
    checkResponseBodyMessage } from "../../helpers/functionsForChecking.helpers";
import { UserController } from "../lib/controllers/user.controller";
const user = new UserController();

describe('Negative Auth (test data)', () => {
    let invalidDataSet = [
        { email: 'subs.test.acc.mos@gmail.com', password: 'qwerty1234' },
        { email: 'subs.test.acc.mosw@gmail.com', password: 'qwerty123' },
        { email: 'subs.test.acc. mos@gmail.com', password: 'qwerty123' },
    ];

    invalidDataSet.forEach((credentials) => {
        it(`N login using: ${credentials.email} + ${credentials.password}`, async () => {
            let response = await user.logIn(credentials);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, "UNAUTHORIZED");
            checkResponseBodyMessage(response, "Bad credentials");
            checkResponseTime(response, 3000);
        });
    })
});