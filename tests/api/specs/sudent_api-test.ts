import { expect } from "chai";
import { checkResponseTime, checkStatusCode, checkResponseSchema } from "../../helpers/functionsForChecking.helpers";
import { ArticlesController } from "../lib/controllers/articles.controller";
import { CoursesController } from "../lib/controllers/courses.controller";
import { StudentController } from "../lib/controllers/student.controller";
import { UserController } from "../lib/controllers/user.controller";
const student = new StudentController();
const user = new UserController();
const courses = new CoursesController();
const article = new ArticlesController();
const schemas = require('./data/shemas_student.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Student controller", () => {
    let token: string;
    let userId: string;
    let id: string;
    let student_data;
    let courseId: string;

    before(`Should get access token`, async () => {
        let response = await user.logIn({ email: 'subs.test.acc.mos@gmail.com', password: 'qwerty123' });
 
        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        token = response.body["accessToken"];
        //console.log(token)
    });

    it(`getUser`, async () => {
        let response = await user.getUser(token);
        
        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseSchema(response, schemas.schema_user);

    });

    it(`getStudent`, async () => {
        let response = await student.getStudent(token);    

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseSchema(response, schemas.schema_student);

        student_data = response.body;
        //console.log(student_data["id"]);
    });

    it(`postStudent`, async () => {
        student_data["job"] = 'qwew';
        let response = await student.postStudent(token, student_data);
        
        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

    });

    it(`getStudent (check update)`, async () => {
        let response = await student.getStudent(token);

        //console.log(response.body);
        //console.log(student_data);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseSchema(response, schemas.schema_student);

    });

    it(`getCourses`, async () => {
        let response = await courses.getCourses(token);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseSchema(response, schemas.schema_courses);;
        
        courseId = response.body[0]["id"];
    });

    it(`postCourseComment`, async () => {
        let response = await courses.postCourseComment(token, {
            "courseId": courseId,
            "id": id,
            "text": "commmment"
        });

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseSchema(response, schemas.schema_course_comment);

    });

    it(`NEGATIVE_postCourseComment_noToken`, async () => {
        let response = await courses.postCourseComment("", {
            "courseId": courseId,
            "id": student_data["id"],
            "text": "commmment"
        });

        //console.log(response.body);

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);

    });

    it(`NEGATIVE_getStudent_noToken`, async () => {
        let response = await student.getStudent("");

        //console.log(response.body);

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);

    });

    it(`NEGATIVE_postArticle_asAtudent`, async () => {
        let article_data = {
            "authorId": student_data["studentId"],
            "authorName": student_data["firstName"] + " " + student_data["lastName"],
            "id": student_data["id"],
            "image": "",
            "name": "Tiiiiitllleeee",
            "text": "texxxxxttttxtxttettxtdtcxvghttdtdx stdtcst txdttvsd tstd dtscgsca"
        }
        let response = await article.postArticle(token, article_data);

        //console.log(response.body);

        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);

    });

    after(function () {
        console.log("Student testing finished.")
    })
});
