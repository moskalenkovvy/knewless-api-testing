import { checkResponseTime, 
    checkStatusCode, 
    checkResponseSchema } from "../../helpers/functionsForChecking.helpers";
    import { CoursesController } from "../lib/controllers/courses.controller";
    import { UserController } from "../lib/controllers/user.controller";
    const user = new UserController();
    const courses = new CoursesController();
    const schemas = require('./data/shemas_student.json');

describe('Negative Comments (test data)', () => {
    let token: string;
    let courseId: string;
    let invalidDataSet = [
        { courseId: "", id: "subs.test.acc.mos@gmail.com", "text": "commmment", },
        { courseId: "id", id: 1222, "text": "commmment", },
        { courseId: "id", id: "id", "text": "commmment", },
    ];

    before(`Should get access token`, async () => {
        let response = await user.logIn({email: 'subs.test.acc.mos@gmail.com', password: 'qwerty123'});
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        token = response.body["accessToken"];
        
        response = await courses.getCourses(token);
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkResponseSchema(response, schemas.schema_courses);
        courseId = response.body[0]["id"];
    });

    invalidDataSet.forEach((credentials) => {
        it(`N post course comment: ${credentials.courseId} + ${credentials.id} + ${credentials.text}`, async () => {
            if (credentials.courseId == "id") { credentials.courseId = courseId; }
            if (credentials.id == "id") { credentials.courseId = courseId; }
            console.log(`CourseId: ${credentials.courseId}`);
            let response = await courses.postCourseComment(token, credentials);

            checkStatusCode(response, 400);
            //checkResponseBodyStatus(response, 'UNAUTHORIZED');
            //checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    })
});
