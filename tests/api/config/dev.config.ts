global.appConfig = {
    envName: 'DEV Environment',
    baseUrl: 'https://knewless.tk/api/',
    swaggerUrl: 'https://knewless.tk/api/swagger-ui/index.html',

    users: {
        TestStudent: {
            email: 'subs.test.acc.mos@gmail.com',
            password: 'qwerty123',
        },
        TestAuthor: {
            email: 'valmos.steam@gmail.com',
            password: 'qwerty123',
        },
    }
}