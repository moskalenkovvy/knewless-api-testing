import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class StudentController {
    async getStudent(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`student`)
            .bearerToken(token)
            .send();
        return response;
    }

    async postStudent(token: string, body_data: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`student`)
            .body(body_data)
            .bearerToken(token)
            .send();
        return response;
    }
}
