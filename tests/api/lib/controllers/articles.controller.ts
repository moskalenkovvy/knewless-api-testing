import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class ArticlesController {
    async postArticle(token: string, body_data: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article`)
            .body(body_data)
            .bearerToken(token)
            .send();
        return response;
    }

    async getMyArticles(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article/author`)
            .bearerToken(token)
            .send();
        return response;
    }

    async getArticleById(token: string, id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article/${id}`)
            .bearerToken(token)
            .send();
        return response;
    }

    async postArticleComment(token: string, body_data: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article_comment`)
            .body(body_data)
            .bearerToken(token)
            .send();
        return response;
    }

    async getArticleComment(token: string, atricleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`article_comment/of/${atricleId}?size=200`)
            .bearerToken(token)
            .send();
        return response;
    }
}
