import { ApiRequest } from "../request";

export class UserController {
    async logIn(body_data: object) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`auth/login`)
            .body(body_data)
            .headers({
                "Content-Type": "application/json",
                "Accept" : "application/json"
            })
            .send();
        return response;
    }

    async getUser(token: string) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`user/me`)
            .bearerToken(token)
            .send();
        return response;
    }
}
