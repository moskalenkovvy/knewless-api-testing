import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class CoursesController {
    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/all`) 
            .send();
        return response;
    }

    async getPopularCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/popular`)
            .send();
        return response;
    }

    async getAllCourseInfoById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/${id}/info`) 
            .send();
        return response;
    }


    async postCourseComment(token: string, body_data: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`course_comment`)
            .body(body_data)
            .bearerToken(token)
            .send();
        return response;
    }

    // As Student
    async getCourses(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/continue`)
            .bearerToken(token)
            .send();
        return response;
    }
}
