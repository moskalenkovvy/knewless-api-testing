import { ApiRequest } from "../request";

let baseUrl: string = "https://knewless.tk/api/";

export class AuthorsController {
    async getAuthor(token: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author`)
            .bearerToken(token)
            .send();
        return response;
    }

    async postAuthor(token: string, body_data: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`author`)
            .body(body_data)
            .bearerToken(token)
            .send();
        return response;
    }

    async getAuthorById(token: string, userId: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/overview/${userId}`)
            .bearerToken(token)
            .send();
        return response;
    }
}
